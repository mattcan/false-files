# False Files

Searches a directory for zero-byte files and returns an error if any are found.

## Usage

```sh
false-files /dir/to/search
```