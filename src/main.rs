extern crate exitcode;

use std::{env, fs, path, io};

fn visit_dirs(dir: &path::Path) -> io::Result<Vec<String>> {
    let mut empty_files: Vec<String> = Vec::new();

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();

        if path.is_dir() {
            match visit_dirs(&path) {
                Ok(mut files) => {
                    empty_files.append(&mut files);
                },
                Err(_) => println!("omfg")
            }
        }

        let metadata = fs::metadata(&path)?;
        if metadata.len() == 0 {
            empty_files.push(path.to_string_lossy().to_string());
        }
    }

    Ok(empty_files)
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        eprintln!("Not enough arguments. Usage:\nff /dir/to/search");
        std::process::exit(exitcode::USAGE)
    }

    let root_dir = path::Path::new(&args[1]);
    if !root_dir.is_dir() {
        eprintln!("Input should be a directory");
        std::process::exit(exitcode::DATAERR);
    }

    match visit_dirs(root_dir) {
        Ok(empty_files) => {
            if empty_files.len() == 0 {
                std::process::exit(exitcode::OK);
            } else {
                eprintln!("{:?}", empty_files);
                std::process::exit(exitcode::SOFTWARE);
            }
        },
        Err(_) => println!("Done goofed")
    }
}
